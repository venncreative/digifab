<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
<!-- COMMON TAGS -->
<meta charset="utf-8">
<title>Digifab - CNC Fabrication and Machining</title>
<!-- Search Engine -->
<meta name="description" content="CNC machining high tech fabrication. Based in Falmouth, working in land, sea and air in the UK and worldwide.">
<meta name="image" content="https://digifab.ltd/assets/img/cnc-machining.jpg">
<!-- Schema.org for Google -->
<meta itemprop="name" content="Digifab - CNC Fabrication and Machining">
<meta itemprop="description" content="CNC machining and high tech fabrication. Based in Falmouth, working in land, sea and air in the UK and worldwide.">
<meta itemprop="image" content="https://digifab.ltd/assets/img/cnc-machining.jpg">
<!-- Twitter -->
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="Digifab - CNC Fabrication and Machining">
<meta name="twitter:description" content="CNC machining high tech fabrication. Based in Falmouth, working in land, sea and air in the UK and worldwide.">
<meta name="twitter:site" content="@digi_fabricate">
<meta name="twitter:image:src" content="https://digifab.ltd/assets/img/cnc-machining.jpg">
<!-- Open Graph general (Facebook, Pinterest & Google+) -->
<meta property="og:title" content="Digifab - CNC Fabrication and Machining">
<meta property="og:description" content="CNC machining and high tech fabrication. Based in Falmouth, working in land, sea and air in the UK and worldwide.">
<meta property="og:image" content="https://digifab.ltd/assets/img/cnc-machining.jpg">
<meta property="og:url" content="https://digifab.ltd/">
<meta property="og:site_name" content="Digifab - CNC Fabrication and Machining">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="website">
<link rel="canonical" href="https://digifab.ltd/" />

 <meta name="robots" content="INDEX, FOLLOW">

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, minimal-ui" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <link rel="stylesheet" href="assets/css/app.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-97575750-1', 'auto');
        ga('send', 'pageview');

    </script>
  </head>

  <body class="body">

$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        if (! this.hasClass('animated-end')) {
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName).addClass('animated-end');
            });
        }

        return this;
    }
});

$(document).ready(function() {

    function setHeights() {
        $('.section').height( $(window).innerHeight() );
    }

    setHeights();

    $(window).on('resize', function() {
        setHeights();
    });


    $('.slideshow .slides').cycle({
        // fx: 'scrollHorz',
        fx: 'fade',
        slides: '.slide',
        pager: '.slideshow-pager',
        pagerTemplate: '<span class="pager"></span>',
        // next: '.carousel-next',
        // prev: '.carousel-prev'
    });


    $(window).on('scroll', function(e) {

        var top = $(window).scrollTop();
        var bottom = $(window).scrollTop() + $(window).height();

        // This made diagonal lines appear for some reason
        // $('#hero .section-overlay').css({
        //     opacity: 1 - (top / $(window).height())
        // });

        $('.js-animate').each(function() {

            if(bottom + 100 > $(this).offset().top) {
                $(this).animateCss( $(this).attr('data-animate') ).removeClass('js-animate');
            }

        });

    });

});

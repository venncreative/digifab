# Starting point for a static website

Here are a set of files from which to start from when making a static site which may or may not become a full blown Wordpress site.

(If it does get that far there will be a script or two to make all the necessary changes to get it to the point of a working Wordpress set up)

## Requirements

* You need [node](http://nodejs.org) installed
* You need [gulp](http://gulpjs.com) installed

## Setup

## Prequisites

Make sure that you have `Mamp` running, it will act as the PHP server for your project while [ Browsersync ](http://browsersync.io) will handle the live reload and fancy stuff via proxy.

### Install Packages

From inside the project folder :
```
npm install
```
(Make a coffee this can take a few minutes but it only needs to be done once at the start)

### Start up Gulp

```
gulp
```
That will run up the Server and give feedback on what is happening with the various processes. Any errors from Sass compilation will show up here, it will also tell you which Url to use for viewing the site as well as for debugging.


<?php

/**
 * Theme Assets
 */

add_action( 'wp_enqueue_scripts', 'venn_enqueue_assets' );

function venn_enqueue_assets() {

    $styles = [
        'font-awesome' => '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'app' => get_template_directory_uri() . '/assets/css/app.min.css',
    ];

    foreach($styles as $name => $file) {
        wp_register_style( $name, $file );
        wp_enqueue_style( $name );
    }

    $scripts_top = [
        // 'dsq-count-scr' => '//radixcommunications.disqus.com/count.js' // had to be added in header
        // 'mixitup' => 'http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js',
        // 'app.head.min' => get_template_directory_uri() . '/assets/js/app.head.min.js'
        'google-maps' => 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false'
    ];

    $scripts_bottom = [
        'cycle2' => '//cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js',
        'addthis' => '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-574317d38c730f61',
        'app.min' => get_template_directory_uri() . '/assets/js/app.min.js',
    ];

    foreach($scripts_top as $name => $file) {
        wp_register_script( $name, $file, array('jquery'), '1.0' );
        wp_enqueue_script( $name );
    }

    foreach($scripts_bottom as $name => $file) {
        wp_register_script( $name, $file, array('jquery'), '1.0', true );
        wp_enqueue_script( $name );
    }
}

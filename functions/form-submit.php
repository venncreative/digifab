<?php

    use Mailgun\Mailgun;

    # Instantiate the client.
    // $mgClient = new Mailgun('key-93e26622d44e0e81815967557f5ea68b');
    // $domain = "sandbox40f9a3150bf843268e107d12314dc38f.mailgun.org";

    $mgClient = new Mailgun('key-856ede602ab6f3ebd4d9d0b320c993a6');
    $domain = "mailgun.digifab.ltd";

    // Name
    $name = $_POST["name"];

    // Email
    $email = $_POST["email"];

    // Phone
    $phone = $_POST["phone"];

    // Your Business Name
    $business_name = $_POST["business_name"];

    // Type of Business
    $type_of_business = $_POST["type_of_business"];

    // Describe
    $project_description = $_POST["project_description"];

    $content = '<table style="width: 600px;">
        <tr style="background-color: lightgrey;">
            <td style=" width: 300px; padding: 5px;">Name : </td>
            <td style="">' . $name . '</td>
        </tr>
        <tr style="background-color: white">
            <td style=" width: 300px; padding: 5px;">Email : </td>
            <td>' . $email . '</td>
        </tr>
        <tr style="background-color: lightgrey;">
            <td style=" width: 300px; padding: 5px;">Phone : </td>
            <td>' . $phone . '</td>
        </tr>
        <tr style="background-color: white;">
            <td style=" width: 300px; padding: 5px;">Business Name : </td>
            <td>' . $business_name . '</td>
        </tr>
        <tr style="background-color: lightgrey;">
            <td style=" width: 300px; padding: 5px;">Type of Business : </td>
            <td>' . $type_of_business . '</td>
        </tr>
        <tr style="background-color: white;">
            <td style=" width: 300px; padding: 5px;">Project Description : </td>
            <td></td>
        </tr>
    </table>
    <table style="width: 600px">
        <tr>
            <td style="width: 600px; padding: 10px;"><p>' . htmlspecialchars($project_description) . '</p></td>
            <td></td>
        </tr>
    </table>';

    date_default_timezone_set('GMT');

    # Make the call to the client.
    $result = $mgClient->sendMessage($domain, array(
        'from'    => $name . ' <postmaster@mailgun.digifab.ltd>',
        'to'      => 'Digifab <Driftwoodsurfboards@gmail.com>',
        'subject' => 'An enquiry from the website – ' . date('D jS F, Y'),
        'text'    => ' ',
        'html'    => '<html>' . $content .'</html>'
    ));

    global $message;
    $message = true;

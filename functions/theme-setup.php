<?php

/**
 * Theme Setup
 */

add_action('after_setup_theme', 'venn_theme_setup' );

function venn_theme_setup() {

    //Set up theme support
    if(function_exists('add_theme_support')) {

        //Add Thumbnail Support
        add_theme_support('post-thumbnails');

        //additional Image Sizes
        if(function_exists('add_image_size')) {
            add_image_size('author_portrait', 350, 350, true);
            add_image_size('author_portrait_small', 180, 180, true);
            add_image_size('work_slide', 620,  450, true);
            //add_image_size('name', width, height, crop);
        }//end if(function_exists('add_image_size'))

        //Add Menus
        add_theme_support('nav-menus');
        add_action('init', 'register_theme_menus');

        //Register Theme Menus
        function register_theme_menus() {

            register_nav_menus(
                array(
                    'main-menu' => __('Main Menu'),
                    'mobile-menu' => __('Mobile Menu')
                )
            );

        }//end register_my_menus()


        //Register Sidebars
        if(function_exists('register_sidebar')) {

            //Sidebar Properties
            $sidebar = array(
                'name' => __('Blog Sidebar'),
                'id' => 'blog-sidebar',
                'before_widget' => '<div class="widget">',
                'after_widget' => '</div>',
                'before_title' => '<h6 class="widget-title">',
                'after_title' => '</h6>',
            );

            //Register Sidebar
            register_sidebar($sidebar);

        }//end if(function_exists('register_sidebar'))

    }//end if(function_exists('add_theme_support'))

}//end mc_theme_setup()

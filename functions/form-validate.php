<?php

    global $v;

    if (!empty($_POST)) {

        // Name
        $name = isset($_POST["name"]) ? $_POST["name"] : null;

        // Email
        $email = isset($_POST["email"]) ? $_POST["email"] : null;

        // Phone
        $phone = isset($_POST["phone"]) ? $_POST["phone"] : null;

        // Your Business Name
        $business_name = isset($_POST["business_name"]) ? $_POST["business_name"] : null;

        // Type of Business
        $type_of_business = isset($_POST["type_of_business"]) ? $_POST["type_of_business"] : null;

        // Describe
        $project_description = isset($_POST["project_description"]) ? $_POST["project_description"] : null;

        $v->addRuleMessage('required', '{field} is required');
        $v->addRuleMessage('alnum', 'Please only use characters A-Z and numbers 0-9');
        $v->addRuleMessage('numeber', 'Please only use numbers 0-9');
        $v->addRuleMessage('email', 'This needs to be a valid email address');

        $v->validate([
            'name| Name'  => [ $name, 'required'],
            'email| Email Address'  => [ $email, 'email|required'],
            'phone| Phone Number' => [ $phone, 'regex(/^[\d\s]+$/)|required'],
            // 'business_name|Business Name'  => [ $business_name, 'required'],
            // 'type_of_business|Type of Business'  => [ $type_of_business, 'alnum'],
            // 'project_description'  => [ $project_description, ''],
        ]);

        global $message;
        $message = false;

        // if validation passes
        if($v->passes()) {
            // send email
            require __DIR__ . '/form-submit.php';
        }

        // if validation fails, page will continue to load and errors will be displayed in form

    };

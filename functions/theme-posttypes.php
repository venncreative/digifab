<?php
/**
 * Post types
 */
include_once __DIR__ . '/../includes/cpt/CPT.php';

$work = new CPT([
    'post_type_name' => 'work',
    'plural' => 'Work',
    'slug' => 'work'
]);

$work->register_taxonomy([
    'taxonomy_name' => 'services',
    'singular' => 'Service',
    'plural' => 'Services',
    'slug' => 'work/service'
]);

$work->menu_icon('dashicons-portfolio');

/**
 * Add Type taxonomy
 */
$blog = new CPT('post');
$blog->register_taxonomy('type');

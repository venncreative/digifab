<?php include 'bootstrap.php'; ?>
<?php include 'header.php'; ?>

    <div id="hero" class="section section--blue">
        <div class="section-overlay section--blue">
            <div class="section-midway">
                <div class="section-content">
                    <div class="row cf">
                        <div class="column two-thirds push-sixth l-one-whole center">

                            <div class="hero-content rellax" data-rellax-speed="7">
                                <img class="logo animated fadeInDown" src="assets/img/logo.svg" />

                                <h2 class="animated fadeInDown">We turn your thought into a thing</h2>

                                <p class="animated fadeInUp">
                                    We are one of the UK’s freshest, most creative CNC fabricators, combining high tech with raw brain power. Projects like a 7 meter sculpture in Dubai, a digitally cut tiny home with an upturned boat for a roof or a table with a mountain relief we love a challenge. We help you create inspiring objects of any scale from idea to installation. We work with clients in the UK and globally to bring brilliant ideas to life. Get in touch.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <img src="assets/img/block-1.svg" class="block-1 rellax animated fadeIn" alt="">
        </div>
    </div>

    <div class="section section--grey-light">
        <div class="section-midway">
            <div class="section-content">

                    <div class="slideshow">
                        <div class="slides">
                            <?php for($i = 1; $i < 5; $i++) : ?>
                                <div class="slide" style="background-image: url('assets/img/slides/slide-<?php echo $i; ?>.jpg');"></div>
                            <?php endfor; ?>
                        </div>
                        <div class="controls">
                            <div class="slideshow-pager"></div>
                        </div>
                    </div>

            </div>
        </div>
    </div>

    <div class="section section--pink">
        <div class="section-midway">
            <div class="section-content">

                <div class="row cf">
                    <div class="column one-whole center">
                        <h2 class="js-animate" data-animate="fadeInDown">How Digifab creates for you</h2>

                        <table class="create-table">
                            <tr>
                                <td>
                                    <div class="create-content create-content-first js-animate" data-animate="fadeInUp">
                                        <h5 class="create-title">1. We Consult</h5>
                                        <p>
                                            Together, we discuss your project. Then agree a brief and budget. Ideally, you supply drawings, sketches or photos. But don’t worry: if you’re not familiar with CNC, we’ll walk you through it.
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="create-content create-content-second js-animate" data-animate="fadeInUp">
                                        <h5 class="create-title">2. We Cut</h5>
                                        <p>
                                            Once the brief is agreed and files checked, our 5-axis CNC machine goes to work. This truly awesome machine cuts many types of sheet materials in 2d and 3d using digitally produced files.
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="create-content create-content-third js-animate" data-animate="fadeInUp">
                                        <h5 class="create-title">3. We Create</h5>
                                        <p>
                                            Depending on your project, we can supply the cut components to you. Or we can assemble and finish for you in our fully-equipped workshop. We collaborate with you to bring your idea to life.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>

            </div>
        </div>
        <img src="assets/img/block-3.svg" class="block-3 rellax" data-rellax-speed="-1" alt="">
    </div>

    <div class="section section--grey-dark section--img" style="background-image: url('assets/img/background.jpg');">
        <div class="section-midway">
            <div class="section-content">
                <div class="row">
                    <div class="column one-whole center">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>

        <img src="assets/img/block-2.svg" class="block-2 rellax" alt="">
    </div>

    <div class="section section--grey-light" id="form">
        <div class="section-midway">
            <div class="section-content">
                <div class="row">
                    <div class="column two-thirds push-sixth l-one-whole center">

                        <h1>Get in touch</h1>
                        <p>What can we create for you? Whether you’re an architect, interior designer,superyacht designer, furniture maker, inventor or entrepreneur, we’d love to hear from you. From our workspace in Falmouth we work with clients around the world - so get in touch by filling in the briefing form or picking up phone. </p>

                        <p>Drop me an email at <a href="mailto:dave@digifab.ltd">dave@digifab.ltd</a>. Describe your project and your CNC requirements. Please also tell us your Business name and the type of business it is.</p>
                        <p>Prefer a chat? Call Digifab on <a href="tel:07814708406">07814 708406</a> to discuss your project</p>

                        <address>
                            26 Tregoniggie Industrial Estate, Falmouth, TR11 4SN
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <iframe
        class="map"
        width="600"
        height="600"
        frameborder="0" style="border:0"
        src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAaf4Va1U_NM6Qf-MP6sQWV_QP3MoxYoBs&q=Digifab,+Empire+Way,+Tregoniggie+Industrial+Estate,+Falmouth"
        allowfullscreen>
    </iframe>

<?php include 'footer.php'; ?>
